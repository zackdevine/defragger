# Defragger
##A digital platforming adventure
------
Defragger was created using Unity 5 and C#.

### Builds
There are compiled builds for Windows 32-bit, Windows 64-bit, and OS X. These builds are located in `(projectdir)/Builds/Release`.

### License
Defragger is open-source and licensed by the GNU GPL v3 license - [See here](http://choosealicense.com/licenses/gpl-3.0/).

### Contributions
Defragger uses music from [Blackmill Music](https://www.youtube.com/user/BlackmillMusic) as well as others.
