﻿using UnityEngine;
using System.Collections;

public class HoldingTrigger : MonoBehaviour {

	private BackAndForthPlatform platform;

	// Use this for initialization
	void Start () {
		platform = transform.parent.gameObject.GetComponent<BackAndForthPlatform> ();
	}

	void OnTriggerEnter(Collider other) {
		platform.AddHeldObject (other.gameObject);
	}

	void OnTriggerExit(Collider other) {
		platform.RemoveHeldObject (other.gameObject);
	}
}
