﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ResetLevel : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    
    void OnTriggerEnter(Collider obj) {
        if (obj.tag == "Player")
            SceneManager.LoadScene(Application.loadedLevelName);
    }
}
