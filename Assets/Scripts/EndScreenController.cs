﻿using UnityEngine;
using System.Collections;

public class EndScreenController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    
    public void Restart() {
        Application.LoadLevel(1);
    }
    
    public void EndGame() {
        Application.LoadLevel(0);
    }
}
