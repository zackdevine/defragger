﻿using UnityEngine;
using System.Collections;

public class PauseMenuController : MonoBehaviour {

  public GameObject pauseMenu;

	// Use this for initialization
	void Start () {
    pauseMenu.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

  public void PauseGame() {
    Time.timeScale = 0;
    pauseMenu.SetActive(true);
  }

  public void ResumeGame() {
    Time.timeScale = 1;
    pauseMenu.SetActive(false);
  }
}
