﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour {
    
    public int maxHealth = 4;
    public int curHealth = 4;
    public float killDelayTime = 2f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    print(this.curHealth + "/" + this.maxHealth);
	}
    
    public void Hit(int amount) {
        this.curHealth -= amount;
        if (this.curHealth <= 0)
            this.Kill();
    }
    
    void Kill() {
        Application.LoadLevel("LevelEnd");
        // Use some death animation or something
        //Destroy(this.gameObject, this.killDelayTime);
    }
}
