﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenuController : MonoBehaviour {
    
    public GameObject mainMenu;
    public GameObject levelChooser;
    public GameObject optionsMenu;
    
    public bool shouldSoundBeOn = true;
    
    void Start() {
        levelChooser.SetActive(false);
        optionsMenu.SetActive(false);
    }
    
    private void PlayButtonClick() {
        if (this.shouldSoundBeOn)
            GameObject.Find("ButtonClick").GetComponent<AudioSource>().Play();
    }
    
    public void StartGame() {
        PlayButtonClick();
        Application.LoadLevel(Application.loadedLevel +1);
    }
    
    public void ChooseLevel() {
        PlayButtonClick();
        mainMenu.SetActive(false);
        levelChooser.SetActive(true);
    }
    
    public void LoadLevel(int levelId) {
        Application.LoadLevel(levelId);
    }
    
    public void ShowOptions() {
        PlayButtonClick();
        mainMenu.SetActive(false);
        optionsMenu.SetActive(true);
    }
    
    public void ReturnToMenu() {
        PlayButtonClick();
        
        levelChooser.SetActive(false);
        optionsMenu.SetActive(false);
        
        mainMenu.SetActive(true);
    }
    
    public void ToggleQuality() {
        PlayButtonClick();
        
        string[] levelnames = {"Fastest", "Fast", "Good", "Beautiful", "Fantastic"};
        int currentLevel = QualitySettings.GetQualityLevel();
        int nextLevel = currentLevel + 1;
        if (currentLevel == 5)
            nextLevel = 0;
            
        SetQualityLevel(nextLevel);
        
        GameObject.Find("QualityButton").GetComponentInChildren<Text>().text = "Quality: " + levelnames[nextLevel].ToString();
    }
    
    private void SetQualityLevel(int level) {
        // 0-5; 0 = Low, 5 = Fantastic
        QualitySettings.SetQualityLevel(level);
    }
    
    public void ToggleSound() {
        PlayButtonClick();
        this.shouldSoundBeOn = !this.shouldSoundBeOn;
    }
    
    public void QuitGame() {
        PlayButtonClick();
        Application.Quit();
    }
    
}
