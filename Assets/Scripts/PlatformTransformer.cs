﻿using UnityEngine;
using System.Collections;

public class PlatformTransformer : MonoBehaviour {

    public Transform pointA;
    public Transform pointB;
    public float speed = 1f;
    public float distanceThreshold = 0.1f;
    public Vector3 velocity;
    
    private bool moveToA = true;

	// Use this for initialization
	void Start () {
	   StartCoroutine(StartMoving());
	}
	
	// Update is called once per frame
	void Update () {
        if (moveToA)
        {
            if ((pointA.position - this.transform.position).magnitude <= this.distanceThreshold)
                moveToA = !moveToA;
        }
        else
        {
            if ((pointB.position - this.transform.position).magnitude <= this.distanceThreshold)
                moveToA = !moveToA;
        }
        this.transform.position += this.velocity * Time.deltaTime;
	}
    
    IEnumerator StartMoving() {
        while (true)
        {
            if (moveToA)
                this.velocity = (pointA.position - this.transform.position).normalized * this.speed;
            else
                this.velocity = (pointB.position - this.transform.position).normalized * this.speed;
                
                
            yield return new WaitForSeconds(this.distanceThreshold / this.speed);
        }
    }
}
