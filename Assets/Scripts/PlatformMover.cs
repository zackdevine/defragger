﻿using UnityEngine;
using System.Collections;

public class PlatformMover : MonoBehaviour {
	private Vector3 moveFromLocation;
	public Vector3 moveToLocation;
	public int pauseTime;

	// Use this for initialization
	void Start () {
		this.moveFromLocation = transform.position;
		StartCoroutine (MovePlatform ());
	}
	
	// Update is called once per frame
	void Update () {

	}

	IEnumerator MovePlatform()
	{
		yield return new WaitForSeconds (this.pauseTime);
		transform.Translate (this.moveToLocation * Time.deltaTime);
		yield return new WaitForSeconds (this.pauseTime);
		transform.Translate(this.moveFromLocation * Time.deltaTime);
	}
}
