﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
  
  public GameObject obj;
  private Vector3 offset;
  
	// Use this for initialization
	void Start () {
    this.offset = transform.position - obj.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
    transform.rotation = obj.transform.rotation;
    transform.position = obj.transform.position + obj.transform.rotation * (Vector3.back * this.offset.magnitude) + (new Vector3(0, this.offset.y, 0));
	}
}
