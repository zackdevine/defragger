﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
    
    public GameObject objectToFollow;
    
    public float cameraDistance;
    public float cameraOffsetX;
    public float cameraOffsetY;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position = new Vector3(objectToFollow.transform.position.x + this.cameraOffsetX, objectToFollow.transform.position.y + this.cameraOffsetY, -this.cameraDistance);
	}
}
