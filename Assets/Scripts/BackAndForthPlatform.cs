﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BackAndForthPlatform : MonoBehaviour {
	
	public Transform pointA;
	public Transform pointB;
	public float speed = 1f;
	public float distanceThreshold = 0.1f;

	private bool moveToA = true;
	public Vector3 velocity;

	private List<GameObject> heldObjects;


	// Use this for initialization
	void Start () {

		heldObjects = new List<GameObject>();

		StartCoroutine (StartMoving());
	}
	
	// Update is called once per frame
	void Update () {
		if (moveToA && (transform.position - pointA.position).magnitude <= distanceThreshold)
			moveToA = !moveToA;
		else if (!moveToA && (transform.position - pointB.position).magnitude <= distanceThreshold)
			moveToA = !moveToA;

		GetComponent<Rigidbody>().position += velocity * Time.deltaTime;

		foreach (GameObject obj in heldObjects) {
			if(obj == null) {
				heldObjects.Remove (obj);
				continue;
			}

			obj.transform.position += velocity * Time.deltaTime;
		}
	}

	public void AddHeldObject(GameObject obj) {
		if (!heldObjects.Contains (obj.transform.root.gameObject))
			heldObjects.Add (obj.transform.root.gameObject);
	}

	public void RemoveHeldObject(GameObject obj) {
		heldObjects.Remove (obj.transform.root.gameObject);
	}

	IEnumerator StartMoving() {
		while(true) {
			if(moveToA)
				velocity = (pointA.position - transform.position).normalized * speed;
			else 
				velocity = (pointB.position - transform.position).normalized * speed;
			yield return new WaitForSeconds(distanceThreshold/speed);
		}
	}

}
