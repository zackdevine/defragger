﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {
    
    public float speedFactor = 0.2f;
    public float jumpPower = 300f;
    public bool isGrounded = true;
    
    public Text fragmentText;
    public Text fragmentTextShadow;
    
    public Text memoryLevel;
    
    public int score = 0;
    public int scoreNeeded;
    
    
    
    private GameObject feetObject;
    
	void Start () {
        GameObject[] frags = GameObject.FindGameObjectsWithTag("Pickup");
        this.scoreNeeded = frags.Length;
        
        this.feetObject = this.transform.FindChild("Feet").gameObject;
    }

    void Update () {
        string updateText = "Fragments: " + (this.scoreNeeded - this.score).ToString();
        this.fragmentText.text = updateText;
        this.fragmentTextShadow.text = updateText;
        
        this.memoryLevel.text = "Memory Level: " + this.GetComponent<PlayerHealth>().curHealth;
    }
  
    void FixedUpdate() {
        // float horiz = Input.GetAxis("Horizontal");
        
        // if (this.GetComponent<Rigidbody>().velocity.y != 0)
        //     this.isGrounded = false;
        // else
        //     this.isGrounded = true;
      
        // if (Input.GetButtonDown("Jump") && this.isGrounded)
        //     this.GetComponent<Rigidbody>().AddForce(Vector3.up * jumpPower);
      
        // if (horiz != 0f)
        //     this.transform.position += new Vector3(horiz * speedFactor, 0, 0);
    }
  
    void OnTriggerEnter(Collider obj)
    {
        if (obj.tag == "Pickup")
        {
            GetComponent<AudioSource>().Play();
            this.score++;
            Destroy(obj.gameObject);   
        }
        
        if (obj.tag == "Exit" && (this.score >= this.scoreNeeded))
        {
            SceneManager.LoadScene(Application.loadedLevel +1);   
        }
        
        if (obj.tag == "ResetZone")
        {
            SceneManager.LoadScene(Application.loadedLevelName);
        }
        
        if (obj.tag == "GameFinish")
        {
            SceneManager.LoadScene("GameWin");
        }
    }
  
}