﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    
    void Kill() {
        this.transform.localScale -= new Vector3(0, 0.3f, 0);
        GetComponent<MoveToRandomWaypoint>().StopMoving();
        transform.GetChild(0).GetComponent<Animator>().enabled = false;
        Destroy(this.gameObject, 2f);
    }
    
    void OnTriggerEnter(Collider obj)
    {
        if (obj.tag == "Feet")
        {
            this.Kill();
        }
        
        if (obj.tag == "Player")
        {
            obj.gameObject.GetComponent<PlayerHealth>().Hit(1);
        }
    }
}
