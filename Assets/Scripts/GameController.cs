﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {
  
  private static GameController instance = null;

  private GameObject pauseMenuController;
  
  public AudioSource asource;
  public List<AudioClip> soundtrack;
  
  public Text trackTitle;
  public Text trackTitleShadow;
  
  public Text levelTitle;
  public Text levelTitleShadow;
  
  private int curTrack = 0;
  
  public static GameController Instance() {
    return instance;
  }
  
  void Awake() {
    if (instance != null)
    {
      DestroyImmediate(gameObject);
      return;
    }
    
    instance = this;
    DontDestroyOnLoad(gameObject);
  }

	// Use this for initialization
	void Start () {
        pauseMenuController = GameObject.Find("PMController");
	}
	
	// Update is called once per frame
	void Update () {
        
        // Reset components
        asource = GetComponent<AudioSource>();
        trackTitle = GameObject.Find("TrackTitle").GetComponent<Text>();
        trackTitleShadow = GameObject.Find("TrackTitleShadow").GetComponent<Text>();
        
        levelTitle = GameObject.Find("LevelTitle").GetComponent<Text>();
        levelTitleShadow = GameObject.Find("LevelTitleShadow").GetComponent<Text>();
        
        string levelTitleText = "// Sector " + Application.loadedLevel;
        levelTitle.text = levelTitleText;
        levelTitleShadow.text = levelTitleText;
        
        if (!asource.isPlaying)
        {
            asource.clip = soundtrack[curTrack];
            asource.Play();
            
            if (curTrack == soundtrack.Count - 1)
                curTrack = 0;
            else
                curTrack++;
        }
        
        int minutes = Mathf.FloorToInt(asource.time / 60F);
        int seconds = Mathf.FloorToInt(asource.time - minutes * 60);
        
        int totalminutes = Mathf.FloorToInt(asource.clip.length / 60F);
        int totalseconds = Mathf.FloorToInt(asource.clip.length - totalminutes * 60);
        
        string niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);
        string niceTotalTime = string.Format("{0:0}:{1:00}", totalminutes, totalseconds);
        
        trackTitle.text = asource.clip.name + "\n" + niceTime + " / " + niceTotalTime;
        trackTitleShadow.text = asource.clip.name + "\n" + niceTime + " / " + niceTotalTime;
        
        if (Input.GetKeyUp(KeyCode.Escape))
            pauseMenuController.GetComponent<PauseMenuController>().PauseGame();
           
        if (Input.GetKeyUp(KeyCode.Slash)) // Change current song
        {
            asource.Stop();
            if (curTrack == soundtrack.Count - 1)
                curTrack = 0;
            else
                curTrack++;
            
            asource.clip = soundtrack[curTrack];
            asource.Play();
        }
	}
}
