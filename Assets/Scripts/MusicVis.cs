﻿using UnityEngine;
using System.Collections;

public class MusicVis : MonoBehaviour {
    public GameObject[] bars;
    public int waveSize = 20;

	// Use this for initialization
	void Start () {}
	
	// Update is called once per frame
	void Update () {
	    float[] spectrum = AudioListener.GetSpectrumData(1024, 0, FFTWindow.Hamming);
        for (int i = 0; i < bars.Length; i++)
        {
            Vector3 prevScale = bars[i].transform.localScale;
            prevScale.y = spectrum[i] * waveSize;
            bars[i].transform.localScale = prevScale;
        }
	}
}
